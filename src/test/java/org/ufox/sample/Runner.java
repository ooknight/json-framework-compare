package org.ufox.sample;

import org.ufox.sample.entity.AllArgsConstructorEntity;
import org.ufox.sample.entity.CircularReference;
import org.ufox.sample.entity.PartArgsConstructorEntity;
import org.ufox.sample.entity.SimpleEntity;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Test;

public class Runner {

    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private ObjectMapper om = new ObjectMapper();

    @Test
    public void from1() {
        from("");
        from("{}");
    }

    @Test
    public void from2() {
        //from("{'id':'1'}");
        from("{\"id\":\"1\"}");
        //from("{'id':'1','name':'demo','date':'2019-01-01','time':'01:02:03.456789','datetime':'2019-01-01T01:02:03.456789'}");
    }

    @Test
    public void to1() {
        SimpleEntity o1 = new SimpleEntity();
        AllArgsConstructorEntity o2 = new AllArgsConstructorEntity(1L, "o1");
        PartArgsConstructorEntity o3 = new PartArgsConstructorEntity(1L);
        to(o1, o2, o3);
    }

    @Test
    public void to2() {
        CircularReference cr = new CircularReference();
        cr.setSuperior(cr);
        System.out.println("是否支持循环引用");
        to(cr);
    }

    private void from(String text) {
        System.out.println(gson.fromJson(text, SimpleEntity.class));
        System.out.println(gson.fromJson(text, AllArgsConstructorEntity.class));
        System.out.println(gson.fromJson(text, PartArgsConstructorEntity.class));
        try {
            System.out.println(om.readValue(text, SimpleEntity.class));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            System.out.println(om.readValue(text, AllArgsConstructorEntity.class));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            System.out.println(om.readValue(text, PartArgsConstructorEntity.class));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void to(SimpleEntity o1, AllArgsConstructorEntity o2, PartArgsConstructorEntity o3) {
        System.out.println(gson.toJson(o1));
        System.out.println(gson.toJson(o2));
        System.out.println(gson.toJson(o3));
        try {
            System.out.println(om.writeValueAsString(o1));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            System.out.println(om.writeValueAsString(o2));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            System.out.println(om.writeValueAsString(o3));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void to(Object o) {
        System.out.println(gson.toJson(o));
        try {
            System.out.println(om.writeValueAsString(o));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
