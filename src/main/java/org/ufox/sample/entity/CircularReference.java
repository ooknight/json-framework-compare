package org.ufox.sample.entity;

import lombok.Data;

@Data
public class CircularReference {

    private CircularReference superior;
}
