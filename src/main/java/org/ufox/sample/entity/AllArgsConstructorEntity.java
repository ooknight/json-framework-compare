package org.ufox.sample.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AllArgsConstructorEntity {

    private Long id;
    private String name;
}
