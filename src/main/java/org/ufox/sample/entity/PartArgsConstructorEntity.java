package org.ufox.sample.entity;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class PartArgsConstructorEntity {

    @NonNull
    private Long id;
    private String name;
}
