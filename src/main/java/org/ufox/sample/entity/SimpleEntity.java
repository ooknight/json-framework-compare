package org.ufox.sample.entity;

import lombok.Data;

@Data
public class SimpleEntity {

    private Long id;
    private String name;
}
